<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Sample Events Class
 *
 * @package
 * @subpackage
 * @category
 * @author
 * @website
 */
class Events_Equipment
{
    
    protected $ci;
    
    public function __construct() {
        $this->ci = & get_instance();
        
        Events::register('get_all_highlow', array(
            $this,
            'get_all_highlow'
        ));
        Events::register('get_all_timers', array(
            $this,
            'get_all_timers'
        ));
        Events::register('get_all_actuators', array(
            $this,
            'get_all_actuators'
        ));
        Events::register('get_all_couplings', array(
            $this,
            'get_all_couplings'
        ));
    }
    
    public function get_all_highlow() {
        $this->ci->load->model('equipment/sensors_m');
        $res = $this->ci->sensors_m->get_all_high_low();
        foreach ($res as $key => $value) {
            if ($value->nrbehindcomma > 0) {
                $res[$key]->nrbehindcomma = (1 / (pow(10, $value->nrbehindcomma)));
            }
        }
        
        return $res;
    }
    
    public function get_all_couplings() {
        $this->ci->load->model('equipment/couplings_m');
        // print_r($this->ci->couplings_m->get_all_tree());
        return $this->ci->couplings_m->get_all_tree();
    }
    
    public function get_all_actuators() {
        $this->ci->load->model('equipment/actuators_m');
        $res = $this->ci->actuators_m->get_all();
        
        //moet de none er echt bij ?
        $temp = array();
        foreach ($res as $value) {
            $temp[$value->id] = $value->name;
        }
        return $temp;
    }
    
    public function get_all_timers() {
        $this->ci->load->model('equipment/timers_m');
        $res = $this->ci->timers_m->get_all();
        $temp = array();
        foreach ($res as $value) {
            $temp[$value->id] = $value->name;
        }
        return $temp;
    }
}

/* End of file events.php */
