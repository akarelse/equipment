<?php if ( isset($extra_message)) { ?>
<div class="alert error"><a href="#" class="close"></a>
	<p><?php echo $extra_message ?></p>
</div>
<?php }?>
<section class="title">
	<h4>
	<?php echo lang('periods:'.$this->method); ?>
	</h4>
</section>
<section class="item">
	<div class="content">
		<?php echo form_open(); ?>
		<div class="tabs">
			<div class="form_inputs" id="page-layout-html">
				<fieldset>
					<ul>
						<li class="even">
							<label for="title"><?php echo lang('global:title');?> <span>*</span></label>
							<?php echo form_input('name', $name, 'maxlength="60"'); ?>
						</li>
						<li class="even">
							<label for="types"><?php echo lang('equipment:types'); ?></label>
							<?php echo form_dropdown('type_id', $types, $type_id) ?>
						</li>
						<li class="even">
							<label for="port"><?php echo lang('equipment:port');?><span>*</span></label>
							<?php echo form_dropdown('port', $ports, $port, ' id="port"'); ?>
						</li>
						<li class="even">
							<label for="binary_pwm"><?php echo lang('equipment:binary_pwm');?><span>*</span></label>
							<?php echo form_dropdown('binary_pwm', array('Binary','PWM'), $binary_pwm, ' id="binary_pwm"'); ?>
						</li>
					</ul>
				</fieldset>
			</div>
		</div>
		<div class="buttons float-right padding-top">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div><?php echo form_close(); ?>
	</div>
</section>