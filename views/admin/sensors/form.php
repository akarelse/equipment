<?php if ( isset($extra_message)) { ?>
<div class="alert error"><a href="#" class="close"></a>
	<p><?php echo $extra_message ?></p>
</div>
<?php }?>

<section class="title">
	<h4>
	<?php echo lang('equipment:'.$this->method); ?>
	</h4>
</section>
<section class="item">
	<div class="content">
		<?php echo form_open(); ?>
		<div class="tabs">
			<div class="form_inputs" id="page-layout-html">
				<fieldset>
					<ul>
						<li class="even">
							<label for="title"><?php echo lang('global:title');?><span>*</span></label>
							<?php echo form_input('name', $name, 'maxlength="60"'); ?>
						</li>


						<div class="input" id="slideranalog" min="0" max="1024">
						</div>
						<li class="even">
						<div id="analog">
							<label for="analogminmax"><?php echo lang('equipment:analogminmax');?><span>*</span></label>
							<?php echo form_input('analogmin', $analogmin, 'maxlength="60"'); ?>
							<?php echo form_input('analogmax', $analogmax, 'maxlength="60"'); ?>
						</li>
						<p id="sliderexplain">Map <span class="ui-icon ui-icon-circle-triangle-n"></span> to <span class="ui-icon ui-icon-circle-triangle-s"></span></p>
						<div class="input" id="sliderdigital" min="-100" max="3000">
						</div>

						<li class="even">
						<div id="digital">
							<label for="digitalminmax"><?php echo lang('equipment:digitalminmax');?><span>*</span></label>
							<?php echo form_input('digitalmin', $digitalmin, 'maxlength="60"'); ?>
							<?php echo form_input('digitalmax', $digitalmax, 'maxlength="60"'); ?>
						</div>
						</li>

						<li class="even">
							<label for="offset"><?php echo lang('equipment:offset');?><span>*</span></label>
							<?php echo form_input('offset', $offset, 'maxlength="60" id="offset"'); ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label><?php echo lang('equipment:sensor_type');?></label>
							<?php echo form_dropdown('type_id', $types, $type_id) ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label><?php echo lang('equipment:bus_id');?></label>
							<?php echo form_dropdown('bus_id', $buses, $bus_id) ?>
						</li>
						<li class="even">
							<label for="port"><?php echo lang('equipment:port');?><span>*</span></label>
							<?php echo form_dropdown('port', $ports, $port ,' id="port"'); ?>
						</li>
						<li class="even">
							<label for="nrbehindcomma"><?php echo lang('equipment:nrbehindcomma');?><span>*</span></label>
							
							<?php echo form_dropdown('nrbehindcomma', array(0,1,2,3), $nrbehindcomma) ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label><?php echo lang('equipment:hasalert');?></label>
							<?php echo form_dropdown('hasalert', array('No','Yes'), $hasalert) ?>
						</li>

					</ul>
				</fieldset>
			</div>
		</div>
		<div class="buttons float-right padding-top">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div><?php echo form_close(); ?>
	</div>
</section>