<section class="title">
	<h4><?php echo lang('equipment:coupling_list'); ?></h4>
</section>
<section class="item">
	<?php if (!empty($items)): ?>
	<?php echo form_open('admin/equipment/couplings/delete'); ?>
	<table>
		<thead>
			<tr>
				<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th><?php echo lang('equipment:name'); ?></th>

				<th></th>
			</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="5">
				<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
			</td>
		</tr>
		</tfoot>
		<tbody>
			<?php foreach( $items as $item ): ?>
			<tr>
				<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
				<td><?php echo $item->name; ?></td>

				<td class="actions">
					<?php echo anchor('admin/equipment/couplings/edit/'.$item->sensor_id, lang('equipment:edit'), 'class="btn orange"') . " " .
					anchor('admin/equipment/couplings/delete/'.$item->sensor_id, lang('equipment:delete'), 'class="btn red"'); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
	</div>
	<?php else: ?>
	<div class="no_data"><?php echo lang('equipment:no_items'); ?></div>
	<?php endif;?>
	<?php echo form_close(); ?>
</section>