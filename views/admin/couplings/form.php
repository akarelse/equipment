<?php if ( isset($extra_message)) { ?>
<div class="alert error"><a href="#" class="close"></a>
<p><?php echo $extra_message ?></p>
</div>
<?php }?>
<section class="title">
<h4>
<?php echo lang('equipment:'.$this->method); ?>
</h4>
</section>
<section class="item">
<div class="content">
	<?php echo form_open(); ?>
	<div class="tabs">
		<div class="form_inputs" id="page-layout-html">
			<fieldset>
				<ul>
					<li class="even">
						<label for="title"><?php echo lang('global:title');?><span>*</span></label>
						<?php echo form_input('name', $name, 'maxlength="60"'); ?>
					</li>
					<?php if(!isset($all_actuators)){ ?>
					<li class="even">
						<label for="sensor"><?php echo lang('equipment:sensor');?></label>
						<?php echo form_dropdown('sensor_id', $sensors ,$sensor_id,' id="sensor"'); ?>
					</li>
					<?php } else { ?>
					<li class="even">
						<label for="sensor"><?php echo lang('equipment:sensor');?></label>
						Not editable.
					</li>
					<?php } ?>

					<?php if(isset($all_actuators)){ 
					foreach ($all_actuators as $key => $value) {?>
					<li class="actuator">
						<label for="actuator">Actuator <?php echo $key ?></label>
						<?php echo form_dropdown('actuator_' . $key, $actuators ,$value); ?>
					</li>
					<?php } } else { ?>
					<li class="actuator">
						<label for="actuator">Actuator 1</label>
						<?php echo form_dropdown('actuator_1', $actuators); ?>
					</li>
					<?php } ?>
					
				</ul>
			</fieldset>
		</div>
	</div>
	<div class="buttons float-right padding-top">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array( 'save_exit', 'cancel') )); ?>
		<?php echo anchor('#', 'Add Actuator', 'class="btn blue" id="addbutton"'); ?>
	</div><?php echo form_close(); ?>
</div>
</section>