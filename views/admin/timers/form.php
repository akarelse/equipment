<section class="title">
	<h4>
	<?php echo lang('equipment:'.$this->method); ?>
	</h4>
</section>
<section class="item">
	<div class="content">
		<?php echo form_open(); ?>
		<div class="tabs">
			<div class="form_inputs" id="page-layout-html">
				<fieldset>
					<ul>
						<li class="even">
							<label for="title"><?php echo lang('global:title');?><span>*</span></label>
							<?php echo form_input('name', $name, 'maxlength="60"'); ?>
						</li>
						<li class="even">
							<label for="slug"><?php echo lang('global:slug');?><span>*</span></label>
							<?php echo form_input('slug', $slug, 'maxlength="60" READONLY'); ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>" id="#inter_date">
							<label><?php echo lang('equipment:ststinter');?></label>
							<?php echo form_dropdown('inter_date', $mode, $inter_date) ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?> interval">
							<label><?php echo lang('equipment:timeunit');?></label>
							<?php echo form_dropdown('inter_quantity', $quantitys, $inter_quantity) ?>
						</li>
						<li class="even interval">
							<label for="slug"><?php echo lang('equipment:amounttime');?></label>
							<?php echo form_input('inter_volume', $inter_volume, 'maxlength="60"'); ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?> date">
							<label><?php echo lang('equipment:startonhour');?></label>
							<?php echo form_dropdown('start_on_hour', $hours, $start_on_hour) ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?> date">
							<label><?php echo lang('equipment:startonminute');?></label>
							<?php echo form_dropdown('start_on_minute', $minutes, $start_on_minute) ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?> date">
							<label><?php echo lang('equipment:stoponhour');?></label>
							<?php echo form_dropdown('stop_on_hour', $minutes, $stop_on_hour) ?>
						</li>
						<li class="<?php echo alternator('', 'even'); ?> date">
							<label><?php echo lang('equipment:stoponminute');?></label>
							<?php echo form_dropdown('stop_on_minute', $minutes, $stop_on_minute) ?>
						</li>

					</ul>
				</fieldset>
			</div>
		</div>
		<div class="buttons float-right padding-top">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div><?php echo form_close(); ?>
	</div>
</section>