<div class="equipment-container">

	{{ if items_exist == false }}
		<p>There are no items.</p>
	{{ else }}
		<div class="equipment-data">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<th>{{ helper:lang line="equipment:name" }}</th>
					<th>{{ helper:lang line="equipment:slug" }}</th>
				</tr>
				<!-- Here we loop through the $items array -->
				{{ itms }}
				<tr>
					<td>{{ name }}</td>
					<td>{{ slug }}</td>
				</tr>
				{{ /items }}
			</table>
		</div>
	
		{{ pagination:links }}
	
	{{ endif }}
	
</div>