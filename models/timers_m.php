<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Timers_m extends MY_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->lists = new StdClass();

        $this->lists->hours = range(1, 24);
        $this->lists->minutes = range(1, 60);
        $this->lists->mode = array(
            'Tijd',
            'Interval',
            'Always',
            'Never'
        );
        $this->lists->quantitys = array(
            'minute',
            'hour',
            'day'
        );

        $this->_table = 'timers';
    }
    
    /**
     * create an timer
     * @param  array $input
     * @return boolean, i guess ?
     */
    public function create($input) {
        $to_insert = array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'inter_date' => $input['inter_date'],
            'inter_quantity' => $input['inter_quantity'],
            'inter_volume' => $input['inter_volume'],
            'start_on_minute' => $input['start_on_minute'],
            'start_on_hour' => $input['start_on_hour'],
            'stop_on_minute' => $input['stop_on_minute'],
            'stop_on_hour' => $input['stop_on_hour']
        );
        
        $to_insert = $this->remove_empty($to_insert);
        $this->db->insert($this->_table, $to_insert);
        return  $this->db->insert_id();
    }

    public function get_all_lists($data)
    {
        $data = (object)array_merge((array)$data,(array)$this->lists);
        return $data;
    }

        private function remove_empty($tofilter)
    {
        foreach ($tofilter as $key => $value) {
            if (empty($value))
            {
                unset($tofilter[$key]);
            }
        }
        return $tofilter;
    }
    
    /**
     * edit a timer item
     * @param   $id
     * @param  array $input
     * @return boolean
     */
    public function edit($id, $input) {
        
        $to_update = array(
            'name' => $input['name'],
            'slug' => $this->_check_slug($input['slug']) ,
            'inter_date' => $input['inter_date'],
            'inter_quantity' => $input['inter_quantity'],
            'inter_volume' => $input['inter_volume'],
            'start_on_minute' => $input['start_on_minute'],
            'start_on_hour' => $input['start_on_hour'],
            'stop_on_minute' => $input['stop_on_minute'],
            'stop_on_hour' => $input['stop_on_hour']
        );
        $to_update = $this->remove_empty($to_update);
        $this->update($id, $to_update);
        
        return ($this->db->affected_rows() > 0)?TRUE:FALSE;
    }
    
    /**
     * check if slug is of ok string format, and return the right format
     * @param  string $slug
     * @return string
     */
    public function _check_slug($slug) {
        $slug = strtolower($slug);
        return preg_replace('/\s+/', '-', $slug);
    }
}
