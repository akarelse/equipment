<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Actuators_m extends MY_Model
{
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->_table = 'actuators';
        $this->lists = new StdClass();
        $this->lists->types = array(
            "Unknown",
            "Pomp",
            "Lamp",
            "Klep",
            "Verwarming",
            "Ventilator",
            "Alert",
            "Bevochtiger",
            "Koeler",
            "System state" // pause, run, stop
        );
        
        $this->lists->ports = $this->get_avalable_ports();
    }
    
    public function get_avalable_ports() {
        $allports = range(0, 52);
        $results = $this->db->select("port")->get($this->_table)->result();
        $results2 = $this->db->select("port")->get("sensors")->result();
        $temp = array();
        foreach ($results as $key => $value) {
            $temp[$value->port] = $value->port;
        }
        foreach ($results2 as $key => $value) {
            $temp[$value->port] = $value->port;
        }
        
        $temp = array_diff($allports, $temp);
        
        return $temp;
    }

    public function get_id_names()
    {
        $results = $this->db->select('name,id')->get($this->_table)->result();
        $temp = array();
        foreach ($results AS $result) {
            $temp[$result->id] = $result->name;
        }
        return $temp;
    }
    
    /**
     * create an period template
     * @param array $input
     * @return boolean
     */
    public function create($input) {
        if (!in_array($input['port'],$this->get_avalable_ports()))
        {
            return false;
        }

        $to_insert = array(
            'name' => $input['name'],
            'slug' => $this->_make_slug($input['type_id'],$input['name']) ,
            'type_id' => $input['type_id'],
            'port' => $input['port'],
            'binary_pwm' => $input['binary_pwm']
        );

        if ($this->db->where("slug ='" . $to_insert['slug'] . "'")->count_all_results($this->_table) > 0)
        {
            return false;
        }
        
        $to_insert = $this->remove_empty($to_insert);
        $this->db->insert($this->_table, $to_insert);
        
        return $this->db->insert_id();
    }
    
    public function get_all_lists($data) {
        
        $data = (object)array_merge((array)$data, (array)$this->lists);
        return $data;
    }


    public function _make_slug($sensor_type,$name)
    {
        $slug = strtolower($this->lists->types[$sensor_type] . "_" . $name);
        return preg_replace('/\s+/', '-', $slug);
    }
    
    private function remove_empty($tofilter) {
        foreach ($tofilter as $key => $value) {
            if (empty($value)) {
                unset($tofilter[$key]);
            }
        }
        return $tofilter;
    }
    
    /**
     * To edit a period template
     * @param type $id
     * @param type $input
     * @return boolean
     */
    public function edit($id, $input) {
        $result = $this->db->select("port")->where("id = '" . $id . "'")->get($this->_table)->row();
        $totest = $this->get_avalable_ports();
        $totest[$result->port] = $result->port;

        if (!in_array($input['port'], $totest))
        {
            return false;
        }

        $to_update = array(
            'name' => $input['name'],
            'slug' => $this->_make_slug($input['type_id'],$input['name']),
            'type_id' => $input['type_id'],
            'port' => $input['port'],
            'binary_pwm' => $input['binary_pwm']
        );

        $result = $this->db->select("id")->where("slug = '" . $to_update['slug'] . "'")->get($this->_table)->row();
        if ($result->id != $id)
        {
            return false;
        }
        
        $to_update = $this->remove_empty($to_update);
        
        $this->update($id, $to_update);
        
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get($id)
    {
        $result = $this->db->select("*")->where("id = " . $id)->get($this->_table)->row();
        if (!empty($result))
        {
            $this->lists->ports[$result->port] = $result->port;
        }
        ksort($this->lists->ports);

        return $result;
    }
}
