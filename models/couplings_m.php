<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Couplings_m extends MY_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->_table = 'couplings';
    }
    
    public function get_all_id($id = 0) {
        $results = $this->db->select('*')->where('sensor_id =' . $id)->get($this->_table)->result();
        if (!empty($results)) {
            $temp = (object)get_object_vars($results[0]);
            unset($temp->actuator_id);
            foreach ($results as $key => $val) {
                $temp->all_actuators[] = $val->actuator_id;
            }
            return $temp;
        }
    }
    
    public function get_all_tree() {
        $results = $this->db->select('*')->group_by('sensor_id,actuator_id,name')->get($this->_table)->result();

        $temp = array();
        foreach ($results as $key => $value) {
            
            if (isset($temp[$value->name]))
            {
                $temp[$value->name]->actuators[] = $this->db->select('*')->where('id =' . $value->actuator_id)->get('actuators')->row();
            }
            else
            {
                $value->actuators[] = $this->db->select('*')->where('id =' . $value->actuator_id)->get('actuators')->row();
                $value->sensor = $this->db->select('*')->where('id =' . $value->sensor_id)->get('sensors')->row();
                unset($value->actuator_id);
                $temp[$value->name] = $value;
            }
        }

        return $temp;
    }
    
    public function get_all() {
        return $this->db->select('*')->group_by('sensor_id')->get($this->_table)->result();
    }
    
    public function create($input) {
        
        // print_r($input);
        $actuators = $this->preg_array_key_exists("/^actuator.*?$/", $input);
        
        // print_r($actuators);
        $ids = array();
        foreach ($actuators as $key => $value) {
            $temp = array(
                'actuator_id' => $value,
                'sensor_id' => $input['sensor_id'],
                'name' => $input['name']
            );
            
            $this->db->insert($this->_table, $temp);
            $ids[] = $this->db->insert_id();
        }
        
        // print_r($actuators);
        return $ids;
    }
    
    function preg_array_key_exists($pattern, $array) {
        $temp = array();
        foreach ($array as $key => $value) {
            if (preg_match($pattern, $key)) {
                $temp[] = $value;
            }
        }
        return $temp;
    }
    
    public function delete($id) {
        $this->db->where('sensor_id =', $id)->delete($this->_table);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
    
    public function edit($id, $input) {
        $actuators = $this->preg_array_key_exists("/^actuator.*?$/", $input);
        
        $this->db->where('sensor_id =', $id)->delete($this->_table);
        foreach ($actuators as $key => $value) {
            
            $temp = array(
                'actuator_id' => $value,
                'sensor_id' => $id,
                'name' => $input['name']
            );
            
            $this->db->insert($this->_table, $temp);
        }
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
}
