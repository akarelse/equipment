<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Sensors_m extends MY_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->_table = 'sensors';
        
        $this->lists = new StdClass();
        $this->lists->types = array(
            "Unknown",
            "Temperatuur",
            "Lucht vocht",
            "Grond vocht",
            "Waterlevel",
            "licht",
            "PH",
            "EC",
            "Alert",
            "CO2"
        );
        
        $this->lists->buses = array(
            "Analog",
            "PWM",
            "I2C",
            "SPI"
        );
        
        $this->lists->ports = $this->get_avalable_ports();
    }
    
    public function get_avalable_ports() {
        $allports = range(0, 52);
        $results = $this->db->select("port")->get($this->_table)->result();
        $results2 = $this->db->select("port")->get("actuators")->result();
        $temp = array();
        foreach ($results as $key => $value) {
            $temp[$value->port] = $value->port;
        }
        foreach ($results2 as $key => $value) {
            $temp[$value->port] = $value->port;
        }
        
        $temp = array_diff($allports, $temp);
        
        return $temp;
    }
    
    /**
     * creates a sensor with the collecting of data , building form and saving.
     * @param  array $input
     * @return ???
     */
    public function create($input) {
        if ($input['analogmax'] == '') {
            $input['analogmax'] = 100;
        }
        if ($input['analogmin'] == '') {
            $input['analogmin'] = 0;
        }
        if ($input['digitalmax'] == '') {
            $input['digitalmax'] = 100;
        }
        if ($input['digitalmin'] == '') {
            $input['digitalmin'] = 0;
        }

        if (!in_array($input['port'],$this->get_avalable_ports()))
        {
            return false;
        }
        
        $to_insert = array(
            'name' => $input['name'],
            'slug' => $this->_make_slug($input['type_id'],$input['name']) ,
            'analogmax' => $input['analogmax'],
            'analogmin' => $input['analogmin'],
            'digitalmax' => $input['digitalmax'],
            'digitalmin' => $input['digitalmin'],
            'offset' => $input['offset'],
            'type_id' => $input['type_id'],
            'bus_id' => $input['bus_id'],
            'port' => $input['port'],
            'nrbehindcomma' => $input['nrbehindcomma'],
            'hasalert' => $input['hasalert']
        );

        if ($this->db->where("slug = '" . $to_insert['slug'] . "'")->count_all_results($this->_table) > 0)
        {
            return false;
        }

        $to_insert = $this->remove_empty($to_insert);
        $this->db->insert($this->_table, $to_insert);
        
        return $this->db->insert_id();
    }
    
    public function get_all_lists($data) {
        $data = (object)array_merge((array)$data, (array)$this->lists);
        return $data;
    }
    
    private function remove_empty($tofilter) {
        foreach ($tofilter as $key => $value) {
            if (empty($value)) {
                unset($tofilter[$key]);
            }
        }
        return $tofilter;
    }
    
    public function _make_slug($type_id,$name)
    {
        $slug = strtolower($this->lists->types[$type_id] . "_" . $name);
        return preg_replace('/\s+/', '-', $slug);
    }
    
    /**
     * edit a sensor item
     * @param  integer $id
     * @param  array $input
     * @return boolean
     */
    public function edit($id, $input) {
        $result = $this->db->select("port")->where("id = " . $id)->get($this->_table)->row();
        $totest = $this->get_avalable_ports();
        $totest[$result->port] = $result->port;

        if (!in_array($input['port'], $totest))
        {
            return false;
        }

        $to_update = array(
            'name' => $input['name'],
            'slug' => $this->_make_slug($input['type_id'],$input['name']) ,
            'analogmax' => $input['analogmax'],
            'analogmin' => $input['analogmin'],
            'digitalmax' => $input['digitalmax'],
            'digitalmin' => $input['digitalmin'],
            'offset' => $input['offset'],
            'type_id' => $input['type_id'],
            'bus_id' => $input['bus_id'],
            'port' => $input['port'],
            'nrbehindcomma' => $input['nrbehindcomma'],
            'hasalert' => $input['hasalert']
        );
        
        $result = $this->db->select("id")->where("slug = '" . $to_update['slug'] . "'")->get($this->_table)->row();
        
        if (!isset($results->id) && $result->id != $id)
        {
            return false;
        }

        $to_update = $this->remove_empty($to_update);
        
        $this->update($id, $to_update);
        
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
    
    /**
     * it should have other name get all sensors, that is what it does, with name index
     * @return array
     */
    public function get_all_high_low() {
        $results = $this->db->select('name,slug,type_id,digitalmax,digitalmin,hasalert,id,nrbehindcomma')->get($this->_table)->result();
        $temp = array();
        foreach ($results AS $rule) {
            $temp[$rule->id] = $rule;
        }
        return $temp;
    }

    public function get_id_names()
    {
        $results = $this->db->select('name,id')->get($this->_table)->result();
        $temp = array();
        foreach ($results AS $result) {
            $temp[$result->id] = $result->name;
        }
        return $temp;
    }
    
    /**
     * get all sensors
     * @return array
     */
    public function get_all() {
        $results = $this->db->get($this->_table)->result();
        return $results;
    }

    public function get($id)
    {
        $result = $this->db->select("*")->where("id = " . $id)->get($this->_table)->row();
        if (!empty($result))
        {
            $this->lists->ports[$result->port] = $result->port;
        }
        ksort($this->lists->ports);

        return $result;
    }
}
