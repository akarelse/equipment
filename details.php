<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Module_Equipment extends Module
{
    
    public $version = '1.0';
    const MIN_PHP_VERSION = '5.2.0';
    const MIN_PYROCMS_VERSION = '2.1';
    const MAX_PYROCMS_VERSION = '2.2.3';
    
    public function info() {
        return array(
            'name' => array(
                'en' => 'Equipment'
            ) ,
            'description' => array(
                'en' => 'This is a PyroCMS module equipment.'
            ) ,
            'frontend' => TRUE,
            'backend' => TRUE,
            'menu' => 'content',
             // You can also place modules in their top level menu. For example try: 'menu' => 'equipment',
            'sections' => array(
                'sensors' => array(
                    'name' => 'equipment:sensors',
                     // These are translated from your language file
                    'uri' => 'admin/equipment/sensors',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'equipment:create_sensor',
                            'uri' => 'admin/equipment/sensors/create',
                            'class' => 'add'
                        )
                    )
                ) ,
                'actuators' => array(
                    'name' => 'equipment:actuators',
                     // These are translated from your language file
                    'uri' => 'admin/equipment/actuators',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'equipment:create_actuator',
                            'uri' => 'admin/equipment/actuators/create',
                            'class' => 'add'
                        )
                    )
                ) ,
                
                'timers' => array(
                    'name' => 'equipment:timers',
                     // These are translated from your language file
                    'uri' => 'admin/equipment/timers',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'equipment:create_timer',
                            'uri' => 'admin/equipment/timers/create',
                            'class' => 'add'
                        )
                    )
                ),
                'couplings' => array(
                    'name' => 'equipment:couplings',
                     // These are translated from your language file
                    'uri' => 'admin/equipment/couplings',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'equipment:create_couplings',
                            'uri' => 'admin/equipment/couplings/create',
                            'class' => 'add'
                        )
                    )
                )
            )
        );
    }
    
    public function install() {
        
        if (!$this->check_php_version()) {
            $this->session->set_flashdata('error', 'Need a higher php version !');
            return FALSE;
        }
        
        if (!$this->check_pyrocms_version()) {
            $this->session->set_flashdata('error', 'Not the right PyroCMS version !');
            return FALSE;
        }
        
        $actuators = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'type_id' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ) ,
            'port' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ) ,
            'binary_pwm' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            )
        );
        
        $sensors = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'type_id' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ) ,
            'bus_id' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ) ,
            'analogmax' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 200
            ) ,
            'analogmin' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 100
            ) ,
            'digitalmax' => array(
                'type' => 'DECIMAL',
                'constraint' => array(
                    5,
                    1
                ) ,
                'default' => 100
            ) ,
            'digitalmin' => array(
                'type' => 'DECIMAL',
                'constraint' => array(
                    5,
                    1
                ) ,
                'default' => - 100
            ) ,
            'port' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 1
            ) ,
            'nrbehindcomma' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 1
            ) ,
            'offset' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ) ,
            'hasalert' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            )
        );
        
        $timers = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'inter_date' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0
            ) ,
            'inter_quantity' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
                'null' => true
            ) ,
            'inter_volume' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
                'null' => true
            ) ,
            'start_on_minute' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
                'null' => true
            ) ,
            'start_on_hour' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
                'null' => true
            ) ,
            'stop_on_minute' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
                'null' => true
            ) ,
            'stop_on_hour' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
                'null' => true
            )
        );
        
        $couplings = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ) ,
            'sensor_id' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
                'null' => true
            ) ,
            'actuator_id' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => 0,
                'null' => true
            )
        );
        
        $this->db->trans_start();
        
        $this->dbforge->add_field($actuators);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('actuators');
        
        $this->dbforge->add_field($sensors);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('sensors');
        
        $this->dbforge->add_field($timers);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('timers');
        
        $this->dbforge->add_field($couplings);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('couplings');
        
        $this->db->trans_complete();
        
        return $this->db->trans_status();
    }
    
    public function uninstall() {
        if ($this->dbforge->drop_table('actuators') && $this->dbforge->drop_table('sensors') && $this->dbforge->drop_table('timers') && $this->dbforge->drop_table('couplings')) {
            $this->pyrocache->delete_all('actuators_m');
            $this->pyrocache->delete_all('sensors_m');
            $this->pyrocache->delete_all('timers_m');
            $this->pyrocache->delete_all('couplings_m');
            return TRUE;
        }
        return FALSE;
    }
    
    public function upgrade($old_version) {
        if (!$this->check_php_version()) {
            $this->session->set_flashdata('error', 'Need a higher php version !');
            return FALSE;
        }
        if (!$this->check_pyrocms_version()) {
            $this->session->set_flashdata('error', 'Not the right PyroCMS version !');
            return FALSE;
        }
        return TRUE;
    }
    
    public function help() {
        
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }
    
    /**
     * Check the current version of PHP and thow an error if it's not good enough
     *
     * @access private
     * @return boolean
     * @author Victor Michnowicz
     */
    private function check_php_version() {
        if (version_compare(PHP_VERSION, self::MIN_PHP_VERSION) < 0) {
            show_error('This addon requires PHP version ' . self::MIN_PHP_VERSION . ' or higher.');
            return FALSE;
        }
        return TRUE;
    }
    
    private function check_pyrocms_version() {
        if (version_compare(CMS_VERSION, self::MIN_PYROCMS_VERSION) < 0 && version_compare(CMS_VERSION, self::MAX_PYROCMS_VERSION) > 0) {
            show_error('This addon requires PYROCMS version ' . self::MIN__VERSION . ' minnimal or version ' . MAX_PYROCMS_VERSION . 'maximal.');
            return FALSE;
        }
        return TRUE;
    }
}

/* End of file details.php */
