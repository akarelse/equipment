<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a equipment module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  equipment Module
 */
class Admin_sensors extends Admin_Controller
{
    protected $section = 'sensors';

    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('sensors_m');
        $this->config->load('config');
        $this->load->library('my_log');
        $this->load->library('form_validation');

        $this->lang->load('equipment');
        $this->item_validation_rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'analogmax',
                'label' => 'Analog max',
                'rules' => 'trim|integer|required'
            ) ,
            array(
                'field' => 'analogmin',
                'label' => 'Analog min',
                'rules' => 'trim|integer|required'
            ) ,
            array(
                'field' => 'digitalmax',
                'label' => 'Max',
                'rules' => 'trim|numeric|required'
            ) ,
            array(
                'field' => 'digitalmin',
                'label' => 'Min',
                'rules' => 'trim|numeric|required'
            ) ,
            array(
                'field' => 'offset',
                'label' => 'Offset',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'type_id',
                'label' => 'Type',
                'rules' => 'trim|integer|required'
            ) ,
            array(
                'field' => 'bus_id',
                'label' => 'Bus',
                'rules' => 'trim|integer|required'
            ) ,
            array(
                'field' => 'port',
                'label' => 'Port',
                'rules' => 'trim|integer|required'
            ) ,
            array(
                'field' => 'nrbehindcomma',
                'label' => 'nrbehindcomma',
                'rules' => 'trim|integer|required'
            ) ,
            array(
                'field' => 'hasalert',
                'label' => 'Has alert',
                'rules' => 'trim|integer|required'
            )
        );

        $this->template->append_js('module::admin.js')->append_js('module::admin-sensor.js')->append_css('module::admin.css')->append_css('module::jquery-ui-1.10.4.custom.min.css')->append_metadata($this->load->view('fragments/wysiwyg'));
    }

    /**
     * index of admin for sensors
     * @return void
     */
    public function index() {
        $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method);
        $data = new StdClass();
        $data->items = $this->pyrocache->model('sensors_m', 'get_all');

        $data = $this->sensors_m->get_all_lists($data);

        if (count($data->items) > 0) {
            foreach ($data->items as $value) {
                $value->type = $data->types[$value->type_id];
            }
        }

        $templatess = Events::trigger('get_all_template_sensors', array() , 'array');

        if (count($templatess) == 1) {
            $templatess = $templatess[0];
        }

        foreach (array_keys($data->items) as $key) {
            if (!in_array($data->items[$key]->id, $templatess))
            {
                $data->items[$key]->deletable = 1;
            }
        }

        $this->template
        ->title($this->module_details['name'])
        ->build('admin/sensors/items',$data);
    }

    /**
     * create a sensor item, the form building filling and saving.
     * @return void
     */
    public function create() {
        $data = new StdClass();
        if ($input = $this->input->post()) {
            $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " POST");
            $this->form_validation->set_rules($this->item_validation_rules);

            if ($this->form_validation->run()) {
                $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " VALIDATED");
                if ($id = $this->sensors_m->create($input)) {

                    $this->pyrocache->delete_all('sensors_m');
                    $this->session->set_flashdata('success', lang('equipment:success'));
                    if ($input['btnAction'] == 'save_exit') {
                        redirect('admin/equipment/sensors');
                    }
                    redirect('admin/equipment/sensors/edit/' . $id);

                }
                $data->extra_message = lang('equipment:error');
            }
        }
        $data = $this->make_fields($input, $data);
        // if (!isset($data['min'])) {
            $data->digitalmin = - 5;
            $data->digitalmax = 20;
            $data->analogmin = 256;
            $data->analogmax = 512;
        // }

        $data = $this->sensors_m->get_all_lists($data);

        $this->template->title($this->module_details['name'], lang('equipment.new_item'))->build('admin/sensors/form', $data);
    }

    private function make_fields($input, $data) {
        foreach ($this->item_validation_rules as $rule) {
            if (isset($input[$rule['field']]))
            {
                $data->{$rule['field']} = $input[$rule['field']];
            }
            else
            {
                $data->{$rule['field']} = "";
            }
        }
        return $data;
    }

    /**
     * edit an sensor, the form building data collection and saving.
     * @param  integer $id
     * @return void
     */
    public function edit($id = 0) {
        $data = $this->sensors_m->get($id);

        if ($input = $this->input->post()) {
            $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " POST");
            $this->form_validation->set_rules($this->item_validation_rules);

            if ($this->form_validation->run()) {
                $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " VALIDATED");
                if ($this->sensors_m->edit($id, $input)) {
                    $this->pyrocache->delete_all('sensors_m');
                    $this->session->set_flashdata('success', lang('equipment:success'));
                    if ($input['btnAction'] == 'save_exit') {
                        redirect('admin/equipment/sensors');
                    }
                    redirect('admin/equipment/sensors/edit/' . $id);

                }
                $data->extra_message = lang('equipment:error');
            }
        }
        $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " FORM");


        $data = $this->sensors_m->get_all_lists($data);

        $this->template->title($this->module_details['name'], lang('equipment.edit'))->build('admin/sensors/form', $data);
    }



    /**
     * delete an sensor if it is not used by an template
     * @param  integer $id
     * @return boolean
     */
    private function delete_if($id = 0) {
        $templatess = Events::trigger('get_all_template_sensors', array() , 'array');

        if (count($templatess) == 1) {
            $templatess = $templatess[0];
        }

        $result = $this->sensors_m->get($id);
        if (in_array($result->name, $templatess)) {
            return false;
        }

        $this->pyrocache->delete_all('sensors_m');
        $this->sensors_m->delete($id);
        return true;

    }

    /**
     * delete stand in for delete_if should be fixed.
     * @param  integer $id
     * @return void
     */
    public function delete($id = 0) {

        if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
            $temp = array();
            foreach ($this->input->post('action_to') as $value) {
                $temp[] = $this->delete_if($value);
            }
            if (in_array(false, $temp)) {
                $this->session->set_flashdata('error', lang('equipment:some_undeleted'));
            } else {
                $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " DELETE");
                $this->session->set_flashdata('success', lang('equipment:success'));
            }
        } elseif (is_numeric($id)) {

            if ($this->delete_if($id)) {
                $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " DELETE MULTYPLE");
                $this->session->set_flashdata('success', lang('equipment:success'));
            } else {
                $this->session->set_flashdata('error', lang('equipment:one_undeleted'));
            }
        }

        redirect('admin/equipment/sensors');
    }

    /**
     * guess
     * @param  array
     * @return array
     */
    function array_flatten($array) {
        $return = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $return = array_merge($return, $this->array_flatten($value));
            } else {
                $return[$key] = $value;
            }
        }
        return $return;
    }
}
