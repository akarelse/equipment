<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  equipment Module
 */
class Admin_couplings extends Admin_Controller
{
    protected $section = 'couplings';
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('couplings_m');
        $this->load->model('actuators_m');
        $this->load->model('sensors_m');
        
        $this->config->load('config');
        $this->load->library('my_log');
        $this->lang->load('equipment');
        
        $this->item_validation_rules = array(
            
            // hoe ga ik dit doen !?
            
        );
        
        $this->initial_fields = array(
            array(
                'field' => 'name',
                'label' => 'Name'
            ) ,
            array(
                'field' => 'sensor_id',
                'label' => 'Sensor'
            ) ,
            array(
                'field' => 'actuator_id',
                'label' => 'Actuator'
            )
        );
        
        $this->template
        ->append_js('module::admin-couplings.js')
        ->append_js('module::admin.js')
        ->append_css('module::admin.css')
        ->append_metadata($this->load->view('fragments/wysiwyg'));
    }
    
    public function index() {
        $data = new StdClass();
        $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method);
        $data->items = $this->couplings_m->get_all();
        //wellicht toch naam aanmaken om deze bij de index weer te kunnen geven ?
        
        $this->template
        ->title($this->module_details['name'])
        ->build('admin/couplings/items', $data);
    }
    
    public function create() {
        $data = new StdClass();
        
        // hier ophalen van alle sensoren en actuatoren, om deze te koppelen.
        // een selectie van of de actuator aangaat op upper sense limit of lower
        // System running state moet ook actuator worden.
        // system running state is running pause en stop
        
        if ($input = $this->input->post()) {

            $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " POST");
            if ($ids = $this->couplings_m->create($input)) {
                $this->pyrocache->delete_all('couplings_m');
                $this->session->set_flashdata('success', lang('equipment:success'));

                if ($input['btnAction'] == 'save_exit') {
                    redirect('admin/equipment/couplings');
                }
                redirect('admin/equipment/couplings/edit/' . $id);
            }
        }
        
        $data = $this->make_fields($input, $data);
        
        $data->actuators = $this->pyrocache->model('actuators_m', 'get_id_names');
        $data->sensors = $this->pyrocache->model('sensors_m', 'get_id_names');
        
        $this->template
        ->title($this->module_details['name'], lang('equipment.new_item'))
        ->build('admin/couplings/form', $data);
    }

    public function edit($id = 0)
    {
        // $data = new StdClass();
        $data = $this->couplings_m->get_all_id($id);
        
        if ($input = $this->input->post()) {
            
            $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " POST");
                $this->pyrocache->delete_all('couplings_m');
                if ($this->couplings_m->edit($id, $input)) {
                    $this->pyrocache->delete('couplings_m');
                    // redirect('admin/equipment/couplings');
                }
        }

        $data->actuators = $this->pyrocache->model('actuators_m', 'get_id_names');
        $data->sensors = $this->pyrocache->model('sensors_m', 'get_id_names');

        $this->template
        ->title($this->module_details['name'], lang('equipment.new_item'))
        ->build('admin/couplings/form', $data);
    }

    public function delete($id)
    {
        $ids = ($id) ? array($id) : $this->input->post('action_to');
        // Go through the array of slugs to delete
        
        if ( ! empty($ids))
        {
            foreach ($ids as $id)
            {
                $this->couplings_m->delete($id);
            }
        }
    $this->pyrocache->delete('couplings_m');
    redirect('admin/equipment/couplings');
    }

    
    private function make_fields($input, $data) {
        foreach ($this->initial_fields as $rule) {
            if (isset($input[$rule['field']])) {
                $data->{$rule['field']} = $input[$rule['field']];
            } else {
                $data->{$rule['field']} = "";
            }
        }
        return $data;
    }
}
