<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Periods Module
 */
class Admin_timers extends Admin_Controller
{
    protected $section = 'timers';


    public function __construct() {
        parent::__construct();
        $this->lang->load('equipment');
        $this->load->model('timers_m');
        $this->config->load('config');
        $this->load->library('my_log');
        $this->load->library('form_validation');
        
        $this->item_validation_rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|max_length[100]|required|is_unique[timers.slug]'
            ) ,
            array(
                'field' => 'inter_date',
                'label' => 'Published',
                'rules' => 'trim|max_length[1]|callback_interordate'
            ) ,
            array(
                'field' => 'inter_quantity',
                'label' => 'inter_unit',
                'rules' => 'trim|max_length[2]'
            ) ,
            array(
                'field' => 'inter_volume',
                'label' => 'inter_volume',
                'rules' => 'trim|max_length[4]'
            ) ,
            array(
                'field' => 'start_on_minute',
                'label' => 'Start on minute',
                'rules' => 'trim|max_length[100]'
            ) ,
            array(
                'field' => 'start_on_hour',
                'label' => 'Start on hour',
                'rules' => 'trim|max_length[100]'
            ) ,
            array(
                'field' => 'stop_on_minute',
                'label' => 'Stop on minute',
                'rules' => 'trim|max_length[100]'
            ) ,
            array(
                'field' => 'stop_on_hour',
                'label' => 'Stop on hour',
                'rules' => 'trim|max_length[100]'
            )
        );
        
        $this->template
        ->append_js('module::admin.js')
        ->append_css('module::admin.css')
        ->append_css('module::jquery-ui-1.10.4.custom.min.css')
        ->append_metadata($this->load->view('fragments/wysiwyg'));
    }
    
    /**
     * Index of timers, big list for admin side
     * @return void
     */
    public function index() {
        $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method);
        $data->items = $this->pyrocache->model('timers_m', 'get_all');
        
        
        foreach (array_keys($data->items) as $key) {
            if (!in_array($data->items[$key]->id, $this->get_temp_timer()))
            {
                $data->items[$key]->deletable = 1;
            } 
        }

        $this->template
        ->title($this->module_details['name'])
        ->build('admin/timers/items', $data);
    }
    
    /**
     * create an timer item with form build collecting data and saving
     * @return void
     */
    public function create() {
        $data = new StdClass();
        if ($input = $this->input->post()) {
            $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method , " POST");
            
            $this->form_validation->set_rules($this->item_validation_rules);
            
            if ($this->form_validation->run()) {
                $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method , " VALIDATED");
                if ($id = $this->timers_m->create($input)) {
                    $this->pyrocache->delete_all('timers_m');
                    $this->session->set_flashdata('success', lang('equipment:success'));
                    if ( $input['btnAction'] == 'save_exit') {
                        redirect('admin/equipment/timers');
                    }
                    redirect('admin/equipment/timers/edit/'.$id);
                    
                }
            }
        }
            $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method , " FORM");
            

        $nowdate = new DateTime();
        $data->start_on_minute = $nowdate->format('i');
        $data->start_on_hour = $nowdate->format('H');
        $nowdate->add(new DateInterval('PT2H10M'));
        $data->stop_on_minute = $nowdate->format('i');
        $data->stop_on_hour = $nowdate->format('H');
        
        $data = $this->make_fields($input, $data);
        
        $data = $this->timers_m->get_all_lists($data);

        $this->template
        ->title($this->module_details['name'], lang('equipment.new_item'))
        ->build('admin/timers/form', $data);
    }

    private function make_fields($input, $data) {
        foreach ($this->item_validation_rules as $rule) {
            if (isset($input[$rule['field']]))
            {
                $data->{$rule['field']} = $input[$rule['field']];
            }
            else
            {
                $data->{$rule['field']} = "";
            }
        }
        return $data;
    }
    
    /**
     * edit a timer item, collect data build form and the saving
     * @param  integer $id
     * @return void
     */
    public function edit($id = 0) {
        $data = $this->timers_m->get($id);
        
        if ($input = $this->input->post()) {
            $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method , " POST");
            if ($input['inter_volume'] == '') {
                $input['inter_volume'] = 0;
            }
            
            $this->form_validation->set_rules($this->item_validation_rules);
            
            if ($this->form_validation->run()) {
                $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method , " VALIDATED");
                $this->pyrocache->delete_all('timers_m');
                if ($this->timers_m->edit($id, $input)) {
                    $this->session->set_flashdata('success', lang('equipment:success'));
                    if ( $input['btnAction'] == 'save_exit') {
                        redirect('admin/equipment/timers');
                    }
                    redirect('admin/equipment/timers/edit/'.$id);
                    
                }
            }
        } 
        $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method , " FORM");
        $nowdate = new DateTime();
        $nowdate->add(new DateInterval('PT2H10M'));
        
        
        $data = $this->timers_m->get_all_lists($data);
        
        $this->template
        ->title($this->module_details['name'], lang('equipment.edit'))
        ->build('admin/timers/form', $data);
    }
    

    
    /**
     * get all timers from template to later compare for deletion check
     * @return array
     */
    private function get_temp_timer() {
        $templatess = Events::trigger('get_all_template_timers', array() , 'array');
        if (count($templatess) == 1) {
            $templatess = $templatess[0];
        }
        return array_unique($templatess);
    }
    
    /**
     * delete an timer if it is no longer used in templates, if used return false enlse true
     * @param  integer $id
     * @return boolean
     */
    private function delete_if($id = 0) {
        $result = $this->timers_m->get($id);

        if (in_array($result->id, $this->get_temp_timer())) {
            return false;
        }

        $this->pyrocache->delete_all('timers_m');
        $this->timers_m->delete($id);
        return true;
        
    }

    /**
     * stand in for delete_if should be combined some time in future.
     * @param  integer $id
     * @return void
     */
    public function delete($id = 0) {
        
        if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
            $temp = array();
            foreach ($this->input->post('action_to') as $value) {
                $temp[] = $this->delete_if($value);
            }
            if (in_array(false, $temp)) {
                $this->session->set_flashdata('error', lang('equipment:some_undeleted'));
            } else {
                $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method , " DELETE MULTYPLE");
                $this->session->set_flashdata('success', lang('equipment:success'));
            }
        } elseif (is_numeric($id)) {
            if ($this->delete_if($id)) {
                $this->my_log->write_log('info',$this->module_details['name'] . " " . $this->controller . " ". $this->method , " DELETE");
                $this->session->set_flashdata('success', lang('equipment:success'));
            } else {
                $this->session->set_flashdata('error', lang('equipment:one_undeleted'));
            }
        }
        
        redirect('admin/equipment/timers');
    }
    
    
    /**
     * A callback function that is not used yet ?
     * @return bolean
     */
    public function interordate() {
        $input = $this->input->post();
        return (isset($input['start_on_minute']) && isset($input['start_on_hour']) && isset($input['stop_on_minute']) && isset($input['stop_on_hour'])) || (isset($input['inter_quantity']) && isset($input['inter_volume']));
    }
}
