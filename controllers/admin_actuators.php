<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a periods module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  equipment Module
 */
class Admin_actuators extends Admin_Controller
{
    protected $section = 'actuators';
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('actuators_m');
        $this->load->model('sensors_m');
        $this->config->load('config');
        $this->load->library('my_log');
        
        $this->load->library('form_validation');
        
        $this->lang->load('equipment');
        $this->item_validation_rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|max_length[100]|required'
            ) ,
            array(
                'field' => 'type_id',
                'label' => 'Type',
                'rules' => 'trim|integer'
            ) ,
            array(
                'field' => 'port',
                'label' => 'Port',
                'rules' => 'trim|integer|required'
            ) ,
            array(
                'field' => 'binary_pwm',
                'label' => 'Binary Pwm',
                'rules' => 'trim|integer|required'
            )
        );
        
        $this->template->append_js('module::admin-actuator.js')->append_js('module::admin.js')->append_css('module::admin.css')->append_metadata($this->load->view('fragments/wysiwyg'));
    }
    
    /**
     * index of actuators, admin list of actuators like, lamps, pumps, etc.
     * @return void
     */
    public function index() {
        $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method);
        $data->items = $this->pyrocache->model('actuators_m', 'get_all');
        
        $data = $this->sensors_m->get_all_lists($data);
        
        if (count($data->items) > 0) {
            foreach ($data->items as $value) {
                $value->type = $data->types[$value->type_id];
            }
        }
        
        foreach (array_keys($data->items) as $key) {
            if (!in_array($data->items[$key]->id, $this->get_temp_actuators()))
            {
                $data->items[$key]->deletable = 1;
            }
        }
        
        $this->template
        ->title($this->module_details['name'])
        ->build('admin/actuators/items',$data);
    }
    
    /**
     * create an actuator item
     * @return void
     */
    public function create() {
        $data = new StdClass();
        if ($input = $this->input->post()) {
            $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " POST");
            
            $this->form_validation->set_rules($this->item_validation_rules);
            
            if ($this->form_validation->run()) {
                $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " VALIDATED");
                
                if ($id = $this->actuators_m->create($input)) {
                    $this->pyrocache->delete_all('actuators_m');
                    $this->session->set_flashdata('success', lang('equipment:success'));
                    if ($input['btnAction'] == 'save_exit') {
                        redirect('admin/equipment/actuators');
                    }
                    redirect('admin/equipment/actuators/edit/' . $id);
                    
                }
            }
            $data->extra_message = lang('equipment:error');
            // $data = $this->make_fields($input, $data);
        }
        $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " FORM");
        $data = $this->make_fields($input, $data);
        
        
        $data = $this->actuators_m->get_all_lists($data);
        $this->template->title($this->module_details['name'], lang('equipment.new_item'))->build('admin/actuators/form', $data);
    }
    
    private function make_fields($input, $data) {
        foreach ($this->item_validation_rules as $rule) {
            if (isset($input[$rule['field']]))
            {
                $data->{$rule['field']} = $input[$rule['field']];
            }
            else
            {
                $data->{$rule['field']} = "";
            }
        }
        return $data;
    }
    
    /**
     * edit an actuator the form building data collecting and save.
     * @param  integer $id
     * @return void
     */
    public function edit($id = 0) {
        $data = $this->actuators_m->get($id);
        if ($input = $this->input->post()) {
            $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " POST");
            
            $this->form_validation->set_rules($this->item_validation_rules);
            
            if ($this->form_validation->run()) {
                $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " VALIDATED");
                
                $this->pyrocache->delete_all('actuators_m');
                if ($this->actuators_m->edit($id, $input)) {
                    if ($input['btnAction'] == 'save_exit') {
                        redirect('admin/equipment/actuators');
                    }
                    redirect('admin/equipment/actuators/edit/' . $id);
                    
                    $this->session->set_flashdata('success', lang('equipment:success'));
                }
                $data->extra_message = lang('equipment:error');
                // $this->session->set_flashdata('error', lang('equipment:error'));
                // redirect('admin/equipment/actuators/create');
                
            }
            $data = $this->make_fields($input, $data);
        }
        $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " FORM");
        
        
        $data = $this->actuators_m->get_all_lists($data);
        // print_r($data);
        $this->template
        ->title($this->module_details['name'], lang('equipment.edit'))
        ->build('admin/actuators/form', $data);
    }
    
    /**
     * helper function the get all actuators that are used by templates.
     * @return array
     */
    private function get_temp_actuators() {
        $templatess = Events::trigger('get_all_template_actuators', array() , 'array');
        
        if (count($templatess) == 1) {
            $templatess = $templatess[0];
        }
        return $templatess;
    }
    
    /**
     * delete a actuator is it is not used by an template
     * @param  integer $id
     * @return boolean
     */
    private function delete_if($id = 0) {
        $result = $this->actuators_m->get($id);
        if (in_array($result->id, $this->get_temp_actuators())) {
            return false;
        }
        $this->pyrocache->delete_all('actuators_m');
        $this->actuators_m->delete($id);
        return true;
        
    }
    
    /**
     * the controller delete method, deletes a period
     * @param type $id
     * @return void
     */
    public function delete($id = 0) {
        
        if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
            $temp = array();
            foreach ($this->input->post('action_to') as $value) {
                $temp[] = $this->delete_if($value);
            }
            if (in_array(false, $temp)) {
                $this->session->set_flashdata('error', lang('equipment:some_undeleted'));
            } else {
                $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " DELETE");
                
                $this->session->set_flashdata('success', lang('equipment:success'));
            }
        } elseif (is_numeric($id)) {
            
            if ($this->delete_if($id)) {
                $this->my_log->write_log('info', $this->module_details['name'] . " " . $this->controller . " " . $this->method, " DELETE MULTYPLE");
                
                $this->session->set_flashdata('success', lang('equipment:success'));
            } else {
                $this->session->set_flashdata('error', lang('equipment:one_undeleted'));
            }
        }
        redirect('admin/equipment/actuators');
    }
}
