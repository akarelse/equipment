<?php
//messages
$lang['equipment:success']		=	'It worked';
$lang['equipment:error']			=	'It didn\'t work';
$lang['equipment:no_items']		=	'No Items';
$lang['equipment:some_undeleted']		=	'some items where not deleted because, the sensors are still used.';
$lang['equipment:one_undeleted']	= 'The sensor is still used, and thus not deleted';

//page titles
$lang['equipment:create_sensor']		=	'Create Sensor';
$lang['equipment:create_actuator']		=	'Create Actuator';
$lang['equipment:create_timer']			=	'Create Timer';

//labels
$lang['equipment:name']				=	'Name';
$lang['equipment:slug']				=	'Slug';
$lang['equipment:manage']			=	'Manage';
$lang['equipment:item_list']		=	'Item List';
$lang['equipment:template_list']	=	'Template List';
$lang['equipment:actuator_list']	=	'Actuator List';
$lang['equipment:sensor_list']		=	'Sensor List';
$lang['equipment:view']				=	'View';
$lang['equipment:edit']				=	'Edit';
$lang['equipment:delete']			=	'Delete';
$lang['equipment:templates']		=	'Templates';
$lang['equipment:starttime']		=	'Start at';
$lang['equipment:stoptime']			=	'Stop at';
$lang['equipment:template']			=	'Template';
$lang['equipment:hours']			=	'hours';
$lang['equipment:minutes']			=	'minutes';
$lang['equipment:types'] 			=	'types';
$lang['equipment:sensors']			=	'Sensors';
$lang['equipment:timers'] 			=	'Timers';
$lang['equipment:minmax'] 			=	'Min - Max';
$lang['equipment:ststinter'] 		=	'Tijd start - stop / Interval';
$lang['equipment:timeunit']			=	'Tijd';
$lang['equipment:amounttime']		=	'Hoeveelheid tijd';
$lang['equipment:startonhour']		=	'Start op uur';
$lang['equipment:startonminute']	=	'Start op minuur';
$lang['equipment:stoponhour']		=	'Stop op uur';
$lang['equipment:stoponminute']		=	'Stop op minuut';
$lang['equipment:bus_id']			=	'Bus Id';
$lang['equipment:port']				=	'Port';
$lang['equipment:hasalert']			=	'Has Alert';
$lang['equipment:binary_pwm']		=	'Binary or PWM';
$lang['equipment:analogminmax']		=	'ADC range';
$lang['equipment:digitalminmax']	=	'Real range';
$lang['equipment:offset']			=	'Offset';

$lang['equipment:sensor_type']	=	'Sensor type';
$lang['equipment:no_templates']	=	'No templates found, make template first.';

$lang['equipment:duration']		=	'Duration days';

//buttons
$lang['equipment:custom_button']	=	'Custom Button';
$lang['equipment:actuators']		=	'Actuators';

?>