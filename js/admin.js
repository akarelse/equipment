jQuery(function($) {
    // generate a slug when the user types a title in
    pyro.generate_slug('input[name="name"]', 'input[name="slug"]');



    $('li [name="inter_date"]').change(function() {
        selected = $(this).find(":selected").text();
        if (selected == "Interval") {
            hidedate();
        } else if (selected == "Tijd") {
            hideinterval();
        } else {
            hideall();
        }
    });



    function hidedate() {
        $("li.date").each(function() {
            $(this).fadeOut(1000);
        });
        $("li.interval").each(function() {
            $(this).fadeIn(1000);
        });
    }

    function hideinterval() {
        $("li.date").each(function() {
            $(this).fadeIn(1000);
        });
        $("li.interval").each(function() {
            $(this).fadeOut(1000);
        });
    }

    function hideall() {
        $("li.date").each(function() {
            $(this).fadeOut(1000);
        });
        $("li.interval").each(function() {
            $(this).fadeOut(1000);
        });
    }

    hashCode = function(str) {
        var hash = 0;
        if (str.length == 0) return hash;
        for (i = 0; i < str.length; i++) {
            char = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
    }

    $('li [name="inter_date"]').change();

    var oldval = calcvalues();


    function calcvalues() {
        var allform = [];
        $.each($("fieldset").find("input"), function() {
            allform.push($(this).val());
        });

        $.each($("fieldset select"), function() {
            allform.push($(this).find(":selected").text());
        });

        return hashCode(allform.join(''));
    }

    $("fieldset").bind("change keyup", function() {
        if (calcvalues() == oldval) {
            $("div .buttons button").each(function() {
                $(this).attr("disabled", "disabled");
            });
        } else {
            $("div .buttons button").each(function() {
                $(this).removeAttr("disabled");
            });
        }

    });

    $("fieldset").keyup();

    $('input[type="text"]').keyup(function(evt){
        var txt = $(this).val();
        $(this).val(txt.replace(/^(.)|\s(.)/g, function($1) {
            return $1.toUpperCase();
        }));
    });


});