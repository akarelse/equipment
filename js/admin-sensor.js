$(document).ready(function() {

    // this is a piece of code from pyro cms to generate slug, this is somewhat double but i needed it.
    generate_slug_better = function(input_form, output_form, space_character, disallow_dashes) {
        var slug, value;
        $(input_form).on('keyup', function() {
            value = $(input_form).val();
            if (!value.length) return;
            space_character = space_character || '-';
            disallow_dashes = disallow_dashes || false;
            var rx = /[a-z]|[A-Z]|[0-9]|[áàâąбćčцдđďéèêëęěфгѓíîïийкłлмñńňóôóпúùûůřšśťтвýыžżźзäæœчöøüшщßåяюжαβγδεέζηήθιίϊκλμνξοόπρστυύϋφχψωώ]/,
                value = value.toLowerCase(),
                chars = pyro.foreign_characters,
                space_regex = new RegExp('[' + space_character + ']+', 'g'),
                space_regex_trim = new RegExp('^[' + space_character + ']+|[' + space_character + ']+$', 'g'),
                search, replace;


            // If already a slug then no need to process any further
            if (!rx.test(value)) {
                slug = value;
            } else {
                value = $.trim(value);

                for (var i = chars.length - 1; i >= 0; i--) {
                    // Remove backslash from string
                    search = chars[i].search.replace(new RegExp('/', 'g'), '');
                    replace = chars[i].replace;

                    // create regex from string and replace with normal string
                    value = value.replace(new RegExp(search, 'g'), replace);
                };



                slug = value.replace(/[^-a-z0-9~\s\.:;+=_]/g, '')
                    .replace(/[\s\.:;=+]+/g, space_character)
                    .replace(space_regex, space_character)
                    .replace(space_regex_trim, '');

                // Remove the dashes if they are
                // not allowed.
                if (disallow_dashes) {
                    slug = slug.replace(/-+/g, '_');
                }
            }

            $(output_form).val(slug);
        });
    }

    $("<input id='hiddentemp' type='hidden' disabled='disabled' name='hiddentemp'  maxlength='60'>").appendTo("div.content form");

    generate_slug_better('input[name="hiddentemp"]', 'input[name="slug"]');

    //maakt de slug door name en type te combineren van sensor edit form
    $("select[name='type_id']").change(function() {
        sensorname = $(this).find(":selected").text();
        typedname = $("div input[name='name']").val();
        $("#hiddentemp").val(sensorname + "_" + typedname);
        $("#hiddentemp").keyup();
    });
    //maakt de slug door name en type te combineren van sensor edit form
    $("div input[name='name']").change(function() {
        typedname = $(this).val();
        sensorname = $("select[name='type_id']").find(":selected").text();
        $("#hiddentemp").val(sensorname + "_" + typedname);
        $("#hiddentemp").keyup();
    });

    //additional slider for sensor max and min value.
    slide = $("#sliderdigital");
    slide.slider({
        range: true,
        min: 0,
        max: 100,
        step: 0.1,
        values: [0, 10],
        slide: function(event, ui) {
            $("#digital").find("[name='digitalmin']").val(ui.values[0]);
            $("#digital").find("[name='digitalmax']").val(ui.values[1]);
            $("fieldset").keyup();
        }

    });

    slide.slider("option", "min", parseFloat(slide.attr('min')));
    slide.slider("option", "max", parseFloat(slide.attr('max')));
    slide.slider("option", "values", [parseFloat($("#digital").find("[name='digitalmin']").val()), parseFloat($("#digital").find("[name='digitalmax']").val())]);

    // additional slider for sensor max and min value.
    aslide = $("#slideranalog");
    aslide.slider({
        range: true,
        min: 0,
        max: 100,
        step: 1,
        values: [0, 10],
        slide: function(event, ui) {
            $("#analog").find("[name='analogmin']").val(ui.values[0]);
            $("#analog").find("[name='analogmax']").val(ui.values[1]);
            $("fieldset").keyup();
        }
    });

    aslide.slider("option", "min", parseFloat(aslide.attr('min')));
    aslide.slider("option", "max", parseFloat(aslide.attr('max')));
    aslide.slider("option", "values", [parseFloat($("#analog").find("[name='analogmin']").val()), parseFloat($("#analog").find("[name='analogmax']").val())]);



    $('li [name="bus_id"]').change(function() {
        selected = $(this).find(":selected").text();
        if (selected == "Analog") {
            Showranges();
        } else {
            Hideranges();
        }
    });

    function Hideranges() {

        // $("#digital").parent().fadeOut(1000);
        $("#analog").parent().fadeOut(1000);
        // $("#sliderdigital").fadeOut(1000);
        $("#slideranalog").fadeOut(1000);
        $("#sliderexplain").fadeOut(1000);
    }

    function Showranges() {
        // $("#digital").parent().fadeIn(1000);
        $("#analog").parent().fadeIn(1000);
        // $("#sliderdigital").fadeIn(1000);
        $("#slideranalog").fadeIn(1000);
        $("#sliderexplain").fadeIn(1000);
    }

    $('li [name="bus_id"]').change();


});